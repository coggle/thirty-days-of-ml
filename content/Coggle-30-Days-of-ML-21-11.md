# Part1 内容介绍

在给大家分享知识的过程中，发现很多同学在学习竞赛都存在较多的问题：

* Linux不会操作，不知道文件目录创建、命令行等细节
* Linux不知道如何运行代码，保存模型
* Pytorch不知道如何从头写代码
* Pytorch不知道定义模型和运行模型
而上述问题都是一个竞赛选手、一个算法工程师所必备的。因此我们将从本月组织一次竞赛训练营活动，希望能够帮助大家入门数据竞赛。在活动中我们将布置具体竞赛任务，然后参与的同学们不断闯关完成，竟可能的帮助大家入门。

11月份的竞赛活动将以下两个内容展开：

* **Linux基础使用**
* **Pytorch与视觉竞赛入门**

---


# Part2 活动安排

* 活动是免费学习活动，不会收取任何费用。
* **请各位同学添加下面微信，并回复【竞赛学习】，即可参与。**

![](https://cdn.coggle.club/coggle666_qrcode.png)


---
# Part3 积分排名和奖励

为了激励各位同学完成的学习任务，将学习任务根据难度进行划分，并根据是否完成进行评分难度高中低的任务分别分数为 3、2 和 1。在完成 11 月学习后，将按照积分顺序进行评选 Top3 的学习者。**如果存在积分相同的情况，则按照打卡博客的原创性、完整性、美观性等角度来衡量。**

如果积分有问题，请联系小助手，总打卡人数：115人

建议所有打卡写到一个博客，每次更新都建议提交打卡

[https://shimo.im/forms/47fosiiAeD42BxOJ/fill](https://shimo.im/forms/47fosiiAeD42BxOJ/fill)

|微信昵称|Linux基础|Pytorch与视觉竞赛|总分|
|:----|:----|:----|:----|
|**推荐Top1：飞羽**|17 任务1-10|26 任务1-12|43|
|**并列Top2/3：梦亦星辰**|17 任务1-10|26 任务1-12|43|
|**并列Top2/3：李宽**|17 任务1-10|26 任务1-12|43|
|**并列Top2/3：徐乜乜**|17 任务1-10|26 任务1-12|43|
|**并列Top2/3：leigang**|17 任务1-10|26 任务1-12|43|
|**并列Top2/3［Mask］**|17 任务1-10|26 任务1-12|43|
|王振华|17 任务1-10|23 任务1-11|40|
|TCSC|17 任务1-10|23 任务1-11|40|
|DeValentin|17 任务1-10|20 任务1-10|37|
|·L·|14|16|30|
|七月听雪|17 任务1-10|11 任务1-7|28|
|Nagaloop|17 任务1-10|11|28|
|宁静致远|5|20|25|
|扁扁|17 任务1-10|5|22|
|吴定俊 |17 任务1-10|5 任务1-4|22|
|4C79|17 任务1-10|4 任务1-3|21|
|Pixel dog|17 任务1-10|2 任务1、2|19|
|liikuan|9 任务1-6|9 任务1-6|18|
|LPF|17 任务1-10|1 任务1|18|
|是馨呀|17 任务1-10| |17|
|Duncan Edwards|17 任务1-10| |17|
|space|17 任务1-10| |17|
|蓝色|17 任务1-10| |17|
|LonelVino|5|9|14|
|邱|9 任务1-6|3|12|
|无盐|9 任务1-6|2 任务1-2|11|
|michaelchuxw| |11|11|
|123|9 任务1-6| |9|
|潘达张 |9 任务1-6| |9|
|子非鱼|9 任务1-6| |9|
|Amihua Lau|7 任务1-5| |7|
|Ceallach_Shaw|7 任务1-5| |7|
|Litra LIN|5|1|6|
|敲完这一行|5| |5|
|daisyn_02|5| |5|
|哈皮|5| |5|
|你真棒|5|0|5|
|哈皮|5| |5|
|一寸光音|5 任务1-4| |5|
|wilson Edwards|5 任务1-4| |5|
|故里 | |5 任务1-4|5|
|郡棋| |4 任务1-3|4|
|茧·约 |3 任务1-3| |3|
|rrren_|3 任务1-3| |3|
|延康| |3|3|
|挽鸽| |2 任务1-2|2|
|Leo| |2|2|
|延康| |2|2|
|李永东| |1|1|
|sapient_river | |1 任务1|1|
|懂礼貌|0| |0|
|海阔天空|5| |0|
|leo|5| |0|
|零到正无穷| | |0|


Top1 的学习者将获得以下**奖励**：

* Coggle & Datawhale 定制周边
* 《机器学习算法竞赛实战》，鱼佬签名版
* Coggle 竞赛专访机会
* 加入 Datawhale 优秀竞赛选手社群
Top2/3 的学习者将获得以下**奖励**：

* Coggle & Datawhale 定制周边
* Coggle 竞赛专访机会
* 加入 Datawhale 优秀竞赛选手社群
注：

* Coggle 数据科学保留活动期间和结束后修改奖励和规则的权利。
* 如果有违反竞赛规则的情况，Coggle 数据科学保留取消相关参赛者的参与排名的权利。

---


# Part4  Linux基础使用

## 学习内容

Linux，全称GNU/Linux，是一种免费使用和自由传播的类UNIX操作系统，其内核由林纳斯·本纳第克特·托瓦兹于1991年10月5日首次发布，它主要受到Minix和Unix思想的启发，是一个基于POSIX的多用户、多任务、支持多线程和多CPU的操作系统。

Linux有上百种不同的发行版，如基于社区开发的debian、archlinux，和基于商业开发的Red Hat Enterprise Linux、SUSE、Oracle Linux等。在全球超级计算机TOP500强操作系统排行榜中，Linux的占比最近十几年长期保持在85%以上，且一直呈现快速上升趋势。根据2016年的排行榜，Linux的占比已经高达98.80%。其实在各种大、中小型企业的服务器应用领域，**在企业内部服务器99%的情况下都是Linux系统，如果你想成为一个合格的软件工程师&算法工程师，Linux是你必备的技能。**

## 打卡汇总

|任务名称|难度|所需技能|
|:----|:----|:----|
|使用命令行登录指定的Linux环境|低、1|ssh|
|在目录下创建文件夹、删除文件夹|低、1|rm、mkdir|
|在目录下下载文件、阅读文件|低、1|wget、nano|
|在目录下使用vi或vim编辑文件|中、2|vi、vim|
|在目录下创建py文件，并进行运行|中、2|python|
|在目录下创建py目录，并进行import导入|中、2|python|
|在Linux系统中后台运行应用程序，并打印日志|中、2|nohup|
|使用grep和sed从文件中筛选字符串|高、3|grep、sed|
|在目录下创建zip和tar压缩文件，并进行解压|中、2|zip、tar|
|使用find和locate定位文件|低、1|find、locate|

## 学习资料

[https://bilibili.com/video/BV1yr4y1C7RC](https://www.bilibili.com/video/BV1yr4y1C7RC)

[https://bilibili.com/video/BV1Zr4y1F7sQ](https://bilibili.com/video/BV1Zr4y1F7sQ)

[https://bilibili.com/video/BV1S64y1v7UG](https://www.bilibili.com/video/BV1S64y1v7UG)

## 打卡要求

**注：**

* **需要使用指定的Linux系统完成打卡任务**
* **需要完成所有的任务细节才算完成一个任务**
**任务1：使用命令行登录指定的Linux环境**

任务要点：ssh登录、密码输入、环境配置

* 步骤1：配置本地登录环境
    * 如果是window系统，安装任意一款ssh工具
        * [https://blog.csdn.net/puss0/article/details/103390947](https://blog.csdn.net/puss0/article/details/103390947)
        * [https://www.runoob.com/linux/linux-remote-login.html](https://www.runoob.com/linux/linux-remote-login.html)
    * 如果是Mac或Linux系统，则不需要，可以直接使用ssh
* 步骤2：使用如下信息登录系统
    * **用户名：coggle，密码：coggle，IP：139.198.15.157**
    * 如果登录失败，请微信联系coggle小助手

**任务2：在目录下创建文件夹、删除文件夹**

任务要点：创建文件夹、创建文件、删除文件、删除文件夹

* 步骤1：学习[Linux的目录结构](https://www.runoob.com/linux/linux-system-contents.html)
* 步骤2：学习[Linux的文件和目录管理](https://www.runoob.com/linux/linux-file-content-manage.html)
* 步骤3：
    * 在/home/coggle目录下，新建一个以你英文昵称（中间不要有空格哦）的文件夹A
    * 在文件夹A内部创建一个以coggle命名的文件夹B
* 步骤4：在B文件夹内创建一个空txt文件
* 步骤5：删除步骤4创建的文件
* 步骤6：删除文件夹B，然后删除文件夹A

**任务3：在目录下下载文件、阅读文件**

任务要点：下载文件、移动文件、阅读文件

* 步骤1：
    * 在home/coggle目录下，新建一个以你英文昵称（中间不要有空格哦）的文件夹A
    * 在文件夹A内部创建一个以coggle命令的文件夹B
* 步骤2：使用wget命令下载[https://mirror.coggle.club/dataset/affairs.txt](https://mirror.coggle.club/dataset/affairs.txt)，到文件夹B
    * wget教程：[https://www.cnblogs.com/pretty-ru/p/10936023.html](https://www.cnblogs.com/pretty-ru/p/10936023.html)
* 步骤3：使用head、cat、tail命令阅读下载的文件。
    * 阅读文件基础教程：[https://www.cnblogs.com/jixp/p/10833801.html](https://www.cnblogs.com/jixp/p/10833801.html)
* 步骤4：在命令行使用ipython进入python3环境，并使用pandas读取下载的文件。

**任务4：在目录下使用vi或vim编辑文件**

任务要点：vi和vim使用

* 步骤1：学习Nano的使用，[https://blog.csdn.net/junxieshiguan/article/details/84104912](https://blog.csdn.net/junxieshiguan/article/details/84104912)
* 步骤2：学习Vim的使用，[https://www.runoob.com/linux/linux-vim.html](https://www.runoob.com/linux/linux-vim.html)
* 步骤3：分别使用Nano和Vim创建py文件，并输入以下内容，并运行
```python
#!/usr/bin/env python3
print('Hello World!')
```

**任务5：在目录下创建py文件，并进行运行**

任务要点：python的os和sys系统接口，文件接口

* 步骤1：学习python下os模块处理文件和目录的函数，[https://www.runoob.com/python/os-file-methods.html](https://www.runoob.com/python/os-file-methods.html)
* 步骤2：学习python下sys模块和传参函数，[https://www.runoob.com/python3/python3-module.html](https://www.runoob.com/python3/python3-module.html)
* 步骤3：在home/coggle目录下，在你英文昵称（中间不要有空格哦）的文件夹中，新建一个test5.py文件，改程序可以使用os、sys模块完成以下功能：
    * 功能1：打印命令行参数
```python
命令行输入：
python3 test5.py 参数1 参数2

程序输出：
test5.py
参数1
参数2
```
    * 功能2：使用os模块打印**/usr/bin/**路径下所有以m开头的文件。
**任务6：在目录下创建py目录，并进行import导入**

任务要点：python代码模块化

* 步骤1：学习python模块化，[https://www.runoob.com/python3/python3-module.html](https://www.runoob.com/python3/python3-module.html)
* 步骤2：在/home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹中创建affairs文件夹。
* 步骤3：编写test6.py和affairs.py完成以下功能：
    * 功能1：affairs.py代码完成[https://mirror.coggle.club/dataset/affairs.txt](https://mirror.coggle.club/dataset/affairs.txt)文件的读取，这里可以直接pd.read_csv('[https://mirror.coggle.club/dataset/affairs.txt](https://mirror.coggle.club/dataset/affairs.txt)')来完成。这一部分建议写为函数。
    * 功能2：test6.py可以导入affairs.py代码
    * 功能3：test6.py可以进行命令行解析，输出[affairs.txt](https://mirror.coggle.club/dataset/affairs.txt)具体的第几行内容。
```python
/home/coggle/
    你英文昵称命名的文件夹/
        test6.py
        affairs/
            affairs.py
```

实现要求：

```python
在/home/coggle/你英文昵称命名的文件夹/目录下，可以执行：

python3 test6.py 10
没有bug，并完成第十行内容的输出。
```

**任务7：在Linux系统中后台运行应用程序，并打印日志**

任务要点：程序后台运行，进程管理

* 步骤1：在/home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹中创建一个sleep.py文件，该文件需要完成以下功能：
    * 程序一直运行
    * 每10秒输出当前时间
* 步骤2：学习 & 和 nohup后台执行的方法
[https://blog.csdn.net/a736933735/article/details/89577557](https://blog.csdn.net/a736933735/article/details/89577557)

[http://ipcmen.com/jobs](http://ipcmen.com/jobs)

* 步骤3：学习tmux的使用，将步骤1的程序进行后台运行，并将输出结果写入到txt文件。
**任务8：使用grep和awk从文件中筛选字符串**

任务要点：字符筛选

* 步骤1：下载周杰伦歌词文本，并进行解压。
[https://mirror.coggle.club/dataset/jaychou_lyrics.txt.zip](https://mirror.coggle.club/dataset/jaychou_lyrics.txt.zip)

* 步骤2：利用grep命令完成以下操作，并输出到屏幕
[https://blog.csdn.net/baidu_41388533/article/details/107610827](https://blog.csdn.net/baidu_41388533/article/details/107610827)

[https://www.runoob.com/linux/linux-comm-grep.html](https://www.runoob.com/linux/linux-comm-grep.html)

    * 统计歌词中 包含【超人】的歌词
    * 统计歌词中 包含【外婆】但不包含【期待】的歌词
    * 统计歌词中 以【我】开头的歌词
    * 统计歌词中 以【我】结尾的歌词
* 步骤3：利用sed命令完成以下操作，并输出到屏幕
[https://www.cnblogs.com/JohnLiang/p/6202962.html](https://www.cnblogs.com/JohnLiang/p/6202962.html)

    * 将歌词中 第2行 至 第40行 删除
    * 将歌词中 所有【我】替换成【你】
**任务9：在目录下创建zip和tar压缩文件，并进行解压**

任务要点：文件压缩

[https://www.cnblogs.com/wxlf/p/8117602.html](https://www.cnblogs.com/wxlf/p/8117602.html)

* 步骤1：在/home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹中，下载[https://mirror.coggle.club/dataset/jaychou_lyrics.txt.zip](https://mirror.coggle.club/dataset/jaychou_lyrics.txt.zip)
* 步骤2：使用zip 压缩/home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹
* 将步骤3：步骤3：将 /home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹，打包为tar格式。
* 步骤4：将 /home/coggle目录下在你英文昵称（中间不要有空格哦）的文件夹，打包为tar.gz格式。
**任务10：使用find和locate定位文件**

任务要点：文件搜索

[https://www.runoob.com/linux/linux-comm-find.html](https://www.runoob.com/linux/linux-comm-find.html)

[https://www.cnblogs.com/linjiqin/p/11678012.html](https://www.cnblogs.com/linjiqin/p/11678012.html)

* 步骤1：使用find统计文件系统中以py为后缀名的文件个数
* 步骤2：使用find寻找/home/文件夹下文件内容包含coggle的文件
* 步骤3：时候用locate寻找到python3.preinst文件

---


# Part5 Pytorch与视觉竞赛入门

## 学习内容

PyTorch的前身是Torch，其底层和Torch框架一样，但是使用Python重新写了很多内容，不仅更加灵活，支持动态图，而且提供了Python接口。它是由Torch7团队开发，是一个以Python优先的深度学习框架，不仅能够实现强大的GPU加速，同时还支持动态神经网络。

PyTorch基本上成了新人进入深度学习领域最常用的框架。**相比于 TensorFlow，PyTorch更易学，也可以更容易的实现自己想要的demo。在本次学习中我们带领大家使用PyTorch来入门深度学习和CV竞赛。**

## 打卡汇总

|任务名称|难度|所需技能|
|:----|:----|:----|
|PyTorch张量计算与Numpy的转换|低、1|PyTorch|
|梯度计算和梯度下降过程|低、1|PyTorch|
|PyTorch全连接层原理和使用|中、2|PyTorch|
|PyTorch激活函数原理和使用|低、1|PyTorch|
|PyTorch卷积层原理和使用|中、2|PyTorch|
|PyTorch常见的损失函数和优化器使用|中、2|PyTorch|
|PyTorch池化层和归一化层|中、2|PyTorch|
|使用PyTorch搭建VGG网络|高、3|PyTorch|
|使用PyTorch搭建ResNet网络|高、3|PyTorch|
|使用PyTorch完成Fashion-MNIST分类|高、3|PyTorch|
|使用PyTorch完成人脸关键点检测|高、3|PyTorch|
|使用PyTorch搭建对抗生成网络|高、3|PyTorch|

## 打卡要求

**任务1：PyTorch张量计算与Numpy的转换**

任务要点：Pytorch基础使用、张量计算

* 步骤1：配置本地Notebook环境，或使用天池DSW：[https://dsw-dev.data.aliyun.com/#/](https://dsw-dev.data.aliyun.com/#/)
* 步骤2：学习Pytorch的基础语法，并成功执行以下代码
    * 基础pytorch教程：[https://zhuanlan.zhihu.com/p/25572330](https://zhuanlan.zhihu.com/p/25572330)
    * 官方教程：[https://pytorch.org/tutorials/beginner/basics/intro.html](https://pytorch.org/tutorials/beginner/basics/intro.html)
```python
c = np.ones((3,3))
d = torch.from_numpy(c)  #numpy 转tensor
```

**任务2：梯度计算和梯度下降过程**

任务要点：Pytorch梯度计算、随机梯度下降

* 步骤1：学习自动求梯度原理，[https://pytorch.org/tutorials/beginner/basics/autogradqs_tutorial.html](https://pytorch.org/tutorials/beginner/basics/autogradqs_tutorial.html)
* 步骤2：学习随机梯度下降原理，[https://www.cnblogs.com/BYRans/p/4700202.html](https://www.cnblogs.com/BYRans/p/4700202.html)
* 步骤3：
    * 使用numpy创建一个$$y=10*x+4+noise(0,1)$$的数据，其中x是0到100的范围，以0.01进行等差数列
    * 使用pytroch定义w和b，并使用随机梯度下降，完成回归拟合。
    **任务3：PyTorch全连接层原理和使用**

任务要点：全连接网络

* 步骤1：学习全连接网络原理，[https://blog.csdn.net/xiaodong_11/article/details/82015456](https://blog.csdn.net/xiaodong_11/article/details/82015456)
* 步骤2：在pytorch中使用矩阵乘法实现全连接层
* 步骤3：在pytorch中使用nn.Linear层
**任务4：PyTorch激活函数原理和使用**

任务要点：激活函数

* 步骤1：学习激活函数的原理，[https://zhuanlan.zhihu.com/p/88429934](https://zhuanlan.zhihu.com/p/88429934)
* 步骤2：在pytorch中手动实现上述激活函数
**任务5：PyTorch卷积层原理和使用**

任务要点：卷积层

* 步骤1：理解卷积层的原理和具体使用
    * [https://blog.csdn.net/qq_37385726/article/details/81739179](https://blog.csdn.net/qq_37385726/article/details/81739179)
    * [https://www.cnblogs.com/zhangxiann/p/13584415.html](https://www.cnblogs.com/zhangxiann/p/13584415.html)
* 步骤2：计算下如下卷积层的参数量
```python
nn.Conv2d(
            in_channels=1,
            out_channels=32,
            kernel_size=5,
            stride=1,
            padding=2
        )
```

**任务6：PyTorch常见的损失函数和优化器使用**

任务要点：损失函数、优化器

* 步骤1：学习损失函数的细节，[https://www.cnblogs.com/wanghui-garcia/p/10862733.html](https://www.cnblogs.com/wanghui-garcia/p/10862733.html)
* 步骤2：学习优化器的使用，[https://pytorch.org/docs/stable/optim.html](https://pytorch.org/docs/stable/optim.html)
* 步骤3：设置不同的优化器和学习率，重复任务2的回归过程
    * 损失函数MSE、优化器SGD、学习率0.1
    * 损失函数MSE、优化器SGD、学习率0.5
    * 损失函数MSE、优化器SGD、学习率0.01
    **任务7：PyTorch池化层和归一化层**

任务要点：池化层、归一化层

* 步骤1：使用pytroch代码实现2d pool中的mean-pooling、max-pooling
    * [https://pytorch.org/docs/stable/nn.html#pooling-layers](https://pytorch.org/docs/stable/nn.html#pooling-layers)
    * [https://blog.csdn.net/shanglianlm/article/details/85313924](https://blog.csdn.net/shanglianlm/article/details/85313924)
* 步骤2：学习归一化的原理，[https://blog.csdn.net/qq_23981335/article/details/106572171](https://blog.csdn.net/qq_23981335/article/details/106572171)
**任务8：使用PyTorch搭建VGG网络**

任务要点：网络搭建

[https://zhuanlan.zhihu.com/p/263527295](https://zhuanlan.zhihu.com/p/263527295)

* 步骤1：理解VGG网络的原理。
* 步骤2：使用pytorch搭建VGG网络模型。
* 步骤3：打印出VGG 11层模型 每层特征图的尺寸，以及参数量。
**任务9：使用PyTorch搭建ResNet网络**

任务要点：网络搭建

[https://zhuanlan.zhihu.com/p/263526658](https://zhuanlan.zhihu.com/p/263526658)

* 步骤1：理解ResNet网络的原理。
* 步骤2：使用pytorch搭建ResNet网络模型。
* 步骤3：打印出ResNet 18模型 每层特征图的尺寸，以及参数量。
**任务10：使用PyTorch完成Fashion-MNIST分类**

[https://github.com/masoudrostami/Fashion-MNIST-using-PyTorch/blob/main/MNIST%20Fashion%20Project.ipynb](https://github.com/masoudrostami/Fashion-MNIST-using-PyTorch/blob/main/MNIST%20Fashion%20Project.ipynb)

* 步骤1：搭建4层卷积 + 2层全连接的分类模型。
* 步骤2：在训练过程中记录下每个epoch的训练集精度和测试集精度。
**任务11：使用PyTorch完成人脸关键点检测**

数据集：[https://ai-contest-static.xfyun.cn/2021/7afa865e-5ac8-48ab-9966-d88bb33cdc15/%E4%BA%BA%E8%84%B8%E5%85%B3%E9%94%AE%E7%82%B9%E6%A3%80%E6%B5%8B%E6%8C%91%E6%88%98%E8%B5%9B_%E6%95%B0%E6%8D%AE%E9%9B%86.zip](https://ai-contest-static.xfyun.cn/2021/7afa865e-5ac8-48ab-9966-d88bb33cdc15/%E4%BA%BA%E8%84%B8%E5%85%B3%E9%94%AE%E7%82%B9%E6%A3%80%E6%B5%8B%E6%8C%91%E6%88%98%E8%B5%9B_%E6%95%B0%E6%8D%AE%E9%9B%86.zip)

[https://gitee.com/coggle/competition-baseline/blob/master/competition/%E7%A7%91%E5%A4%A7%E8%AE%AF%E9%A3%9EAI%E5%BC%80%E5%8F%91%E8%80%85%E5%A4%A7%E8%B5%9B2021/%E4%BA%BA%E8%84%B8%E5%85%B3%E9%94%AE%E7%82%B9%E6%A3%80%E6%B5%8B%E6%8C%91%E6%88%98%E8%B5%9B/face-keypoint2.ipynb](https://gitee.com/coggle/competition-baseline/blob/master/competition/%E7%A7%91%E5%A4%A7%E8%AE%AF%E9%A3%9EAI%E5%BC%80%E5%8F%91%E8%80%85%E5%A4%A7%E8%B5%9B2021/%E4%BA%BA%E8%84%B8%E5%85%B3%E9%94%AE%E7%82%B9%E6%A3%80%E6%B5%8B%E6%8C%91%E6%88%98%E8%B5%9B/face-keypoint2.ipynb)

* 步骤1：搭建4层卷积 + 2层的模型完成关键点回归。
* 步骤2：使用resnet18预训练模型完成关键点回归。
**任务12：使用PyTorch搭建对抗生成网络**

* 步骤1：学习对抗生成网络的原理，[https://blog.csdn.net/DFCED/article/details/105175097](https://blog.csdn.net/DFCED/article/details/105175097)
* 步骤2：学习DCGAN的代码实现，[https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html](https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html)
* 步骤3：使用任务11中的人脸数据（缩放至64*64），并使用DCGAN完成训练，生成人脸。
**注：**

* **需要使用Python环境下Notebook完成以下任务**
* **需要完成所有的任务细节才算完成一个任务**
* **所有的任务可以写在一个Notebook内**

---


# Part6 提问&回答

问：具体的活动是怎么安排的？

>有任务，自己先尝试，然后之后会视频演示和讨论。
问：本次活动是收费的吗，最终奖品如何发放？

>活动是免费的，最终奖品按照积分排行Top3进行发放，如果排名有并列都发送奖励。
问：环境和配置是什么？

>Linux上进行学习，python3环境


